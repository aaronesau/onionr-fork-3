ONIONR_HOME ?= data
all:;: '$(ONIONR_HOME)'

PREFIX = /usr/local

.DEFAULT_GOAL := setup

setup:
	pip3 install -r requirements.txt --require-hashes --user
	-@cd onionr/static-data/ui/; ./compile.py


.PHONY: install uninstall setup
install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin $(DESTDIR)$(PREFIX)/share/onionr
	cp -rfp requirements.txt src static-data scripts $(DESTDIR)$(PREFIX)/share/onionr
	printf '#!/bin/sh\nexport ORIG_ONIONR_RUN_DIR=$(PREFIX)/share/onionr\n' > $(DESTDIR)$(PREFIX)/bin/onionr
	cat onionr.sh >> $(DESTDIR)$(PREFIX)/bin/onionr
	chmod +x $(DESTDIR)$(PREFIX)/bin/onionr

uninstall:
	rm -rf $(DESTDIR)$(PREFIX)/share/onionr
	rm -f $(DESTDIR)$(PREFIX)/bin/onionr

test:
	./run_tests.sh

soft-reset:
	@echo "Soft-resetting Onionr..."
	./onionr.sh soft-reset
	@./onionr.sh version | grep -v "Failed" --color=always

reset:
	@echo "Hard-resetting Onionr..."
	rm -rf $(ONIONR_HOME)/ | true > /dev/null 2>&1
	cd onionr/static-data/www/ui/; rm -rf ./dist; python compile.py
	#@./onionr.sh version | grep -v "Failed" --color=always

plugins-reset:
	@echo "Resetting plugins..."
	@./onionr.sh reset-plugins | true > /dev/null 2>&1
	@./onionr.sh version | grep -v "Failed" --color=always


##########
# Package building

# Basic package information
PKG_NAME=onionr
PKG_DESCRIPTION="Private P2P communication platform"
PKG_VERSION:=$(shell dpkg-parsechangelog -S Version | sed -rne 's,([^-\+]+)+(\+dfsg)*.*,\1,p'i)
UPSTREAM_PACKAGE:=$(PKG_NAME)_${PKG_VERSION}.orig.tar.gz
PKG_RELEASE=0
PKG_MAINTAINER="Aaron Esau \<contact@aaronesau.com\>"
PKG_ARCH=x86_64
PKG_ARCH_RPM=x86_64

# These vars probably need no change
PKG_DEB=../${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}_${PKG_ARCH}.deb
PKG_RPM=../${PKG_NAME}-${PKG_VERSION}-${PKG_RELEASE}.${PKG_ARCH_RPM}.rpm
FPM_OPTS=-s dir -n $(PKG_NAME) -v $(PKG_VERSION) --iteration $(PKG_RELEASE) -C $(TMPINSTALLDIR) --maintainer ${PKG_MAINTAINER} --description $(PKG_DESCRIPTION) -a $(PKG_ARCH) --after-install debian/postinst
TMPINSTALLDIR=/tmp/$(PKG_NAME)-fpm-install

dpkg:
	tar cafv ../${UPSTREAM_PACKAGE} --exclude debian --exclude .git . 

# Generate a deb package using fpm
deb:
	rm -rf $(TMPINSTALLDIR)
	rm -f $(PKG_DEB)
	#chmod -R g-w *	
	make install DESTDIR=$(TMPINSTALLDIR)
	fpm -t deb -p $(PKG_DEB) $(FPM_OPTS) \
		usr

# Generate a rpm package using fpm
rpm:
	rm -rf $(TMPINSTALLDIR)
	rm -f $(PKG_RPM)
	make CFLAGS="$(CFLAGS) -static" CCFLAGS="$(CCFLAGS) -static"
	chmod -R g-w *	
	make install DESTDIR=$(TMPINSTALLDIR)
	fpm -t rpm -p $(PKG_RPM) $(FPM_OPTS) \
		usr


