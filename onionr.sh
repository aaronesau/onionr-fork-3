#!/bin/sh
export ORIG_ONIONR_RUN_DIR="${ORIG_ONIONR_RUN_DIR:-"$(pwd)"}"
export PYTHONDONTWRITEBYTECODE=1
cd "${ORIG_ONIONR_RUN_DIR}/src"
./__init__.py "$@"
